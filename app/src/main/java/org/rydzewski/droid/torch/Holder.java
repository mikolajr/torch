package org.rydzewski.droid.torch;


import android.graphics.SurfaceTexture;
import android.hardware.Camera;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class Holder {

    private Camera camera;
    private AtomicBoolean flashOn = new AtomicBoolean(false);
    private LockTemplate lock = new LockTemplate(new ReentrantLock());
    private SurfaceTexture surfaceTexture;

    public boolean getFlashOn() {
        return flashOn.get();
    }

    private void setFlashOn(boolean flashOn) {
        this.flashOn.set(flashOn);
    }

    public void openCamera() {
        lock.doInLock(new Runnable() {
            @Override
            public void run() {
                camera = Camera.open();
            }
        });
    }

    public void releaseCamera() {
        lock.doInLock(new Runnable() {
            @Override
            public void run() {
                if (camera != null) {
                    camera.release();
                    camera = null;
                }
            }
        });
    }

    public void turnLightOff() {
        lock.doInLock(new Runnable() {
            @Override
            public void run() {
                try {
                    if (camera == null) {
                        return;
                    }
                    if (checkIfFlashIsOn()) {
                        Camera.Parameters parameters = camera.getParameters();
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameters);
                        camera.stopPreview();
                        if (surfaceTexture != null) {
                            surfaceTexture.release();
                            surfaceTexture = null;
                        }
                    }
                } finally {
                    setFlashOn(checkIfFlashIsOn());
                }
            }
        });
    }

    public void turnLightOn() {
        lock.doInLock(new Runnable() {
            @Override
            public void run() {
                try {
                    if (camera == null) {
                        return;
                    }
                    if (!checkIfFlashIsOn()) {
                        Camera.Parameters parameters = camera.getParameters();
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        camera.setParameters(parameters);
                        try {
                            surfaceTexture = new SurfaceTexture(0);
                            camera.setPreviewTexture(surfaceTexture);
                        } catch (IOException ignored) {
                        }
                        camera.startPreview();
                    }
                } finally {
                    setFlashOn(checkIfFlashIsOn());
                }
            }
        });
    }

    /*
    we are inside lock,
    camera is not null
     */
    private boolean checkIfFlashIsOn() {
        return Camera.Parameters.FLASH_MODE_TORCH.equals(camera.getParameters().getFlashMode());
    }

    public void toggleFlash() {
        lock.doInLock(new Runnable() {
            @Override
            public void run() {
                try {
                    if (camera == null) {
                        return;
                    }
                    if (checkIfFlashIsOn()) {
                        turnLightOff();
                    } else {
                        turnLightOn();
                    }

                } finally {
                    setFlashOn(checkIfFlashIsOn());
                }
            }
        });
    }
}
