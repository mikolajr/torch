package org.rydzewski.droid.torch;


public class LightOnTask extends CameraAsyncTask{
    @Override
    protected void doIt() {
        holder.turnLightOn();
    }
}
