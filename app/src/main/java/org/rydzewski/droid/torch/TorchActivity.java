package org.rydzewski.droid.torch;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TorchActivity extends Activity {

    private Holder holder = new Holder();
    private boolean hasFlash;
    private Button torchButton;
    private Button failButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_torch);

        torchButton = (Button) findViewById(R.id.torch_button);
        failButton = (Button) findViewById(R.id.fail_button);

        checkForFlash();
    }

    private void checkForFlash() {
        hasFlash = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!hasFlash) {
            torchButton.setVisibility(View.INVISIBLE);
            failButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (hasFlash) {
            turnLightOff(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (hasFlash) {
            turnLightOn();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (hasFlash) {
            holder.openCamera();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        holder.releaseCamera();
    }

    public void buttonPressed(View view) {
        new ToggleFlashTask(torchButton).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, holder, torchButton, getResources());
    }

    private void turnLightOn() {
        new LightOnTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, holder, torchButton, getResources());
    }

    private void turnLightOff(boolean doNotSetButtonLabel) {
        new LightOffTask(doNotSetButtonLabel).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, holder, torchButton, getResources());
    }

    public void failButtonPressed(View view) {
        finish();
    }
}
