package org.rydzewski.droid.torch;


public class LightOffTask extends CameraAsyncTask {

    public LightOffTask(boolean ignoreButtonAfter) {
        super(ignoreButtonAfter);
    }

    @Override
    protected void doIt() {
        holder.turnLightOff();
    }
}
