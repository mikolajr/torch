package org.rydzewski.droid.torch;


import android.widget.Button;

public class ToggleFlashTask extends CameraAsyncTask {
    public ToggleFlashTask(Button torchButton) {
        torchButton.setEnabled(false);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        torchButton.setEnabled(true);
    }

    @Override
    protected void doIt() {
        holder.toggleFlash();
    }
}
