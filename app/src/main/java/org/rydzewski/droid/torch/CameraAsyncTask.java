package org.rydzewski.droid.torch;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.Button;

public abstract class CameraAsyncTask extends AsyncTask<Object, Void, Void>{

    protected Holder holder;
    protected Button torchButton;
    private Resources resources;
    private boolean ignoreButtonAfter;

    public CameraAsyncTask() {
    }

    public CameraAsyncTask(boolean ignoreButtonAfter) {
        this.ignoreButtonAfter = ignoreButtonAfter;
    }

    @Override
    protected Void doInBackground(Object... contexts) {
        this.holder = (Holder) contexts[0];
        this.torchButton = (Button) contexts[1];
        this.resources = (Resources) contexts[2];
        doIt();
        return null;
    }

    protected abstract void doIt();

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            if (!ignoreButtonAfter) {
                torchButton.setText(resources.getString(holder.getFlashOn() ? R.string.torch_off : R.string.torch_on));
            }
        } catch (Exception ignored) {
        }
    }
}
