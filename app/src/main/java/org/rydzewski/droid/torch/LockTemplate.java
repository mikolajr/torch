package org.rydzewski.droid.torch;

import java.util.concurrent.locks.Lock;

public class LockTemplate {

    private final Lock lock;

    public LockTemplate(Lock lock) {
        this.lock = lock;
    }

    public void doInLock(Runnable runnable) {
        lock.lock();
        try {
            runnable.run();
        } finally {
            lock.unlock();
        }
    }
}
